function Jcost = cost(w) 
%
%      Input:
%      tobs - observation frequency
%      nobs - no of observation
%      wo - observations = truth + random error
%      hh - observation operator
%      wa - initial background state
%      Binv - inverse of background error cov matrix
%      rr - obs error variance
%     
%      Output:
%      cost - cost function
%      kcnt - 

global nobs tobs hh rrinv rrinvsq Binv wobs nobrep
global windowdi windowdf wb KK F dt
wi    = w;
wbin  = w;
bdif  = wi-wbin;

Jbkg = 0.5 * bdif' * Binv * bdif;
Jcost = Jbkg;
it    = int64(windowdi/(dt*tobs));

% disp('Im being used:)')

for t = windowdi:dt:windowdf
%  --- get trajectory from initial state iterate---
   w = lorenz40_solver(w, KK, F, dt);

%  --- If obs available, add contribution to cost ---
   if ( mod(t,tobs) <= 1e-5),
      wo = wobs(1:nobs,int64(t/tobs));
      innov = wo-hh*w;
      % Complete and uncomment the following line
      Jobs = 0.5 * innov' * rrinv * innov;
      Jcost = Jcost + Jobs;
      it = it + 1;
   end  
end   %  End Main Time Loop

% Jcost
