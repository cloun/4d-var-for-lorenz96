
%Code to test the adjoint of the Lorenz 40-component model (which isnt necessarily 40 components)
	% this code runs 2 trajectories with slightly different initial conditions.
	% Saroja Polavarapu Feb. 15, 2004

clear all;
echo off;

KK = 36;				%no. of gridpoints along a latitude circle (Grid spacing of 10 deg.)
F = 8.0;				%forcing
dt = 0.05;			%time step (6 hours or 0.25 days)
iend = 20;			%total number of timesteps to run
Tend = iend*dt;	%length of integration in nondimensional time units
%To convert time to days, multiply by 5
%To convert time to years, divide by 72
cday=5.0;			%factor to convert time to days
cyear=1.0/72.0;	%factor to convert time to years

%--generate some random initial conditions
X0 = 2+randn(KK,1);
es = 0.1;
pert=es*randn(size(X0));
X0pert = X0+pert;

%---first get the background trajectory:
t = 0:dt:Tend;
xx = zeros(KK,length(t))+NaN;
xx(:,1) = X0;
for i = 2:1:iend;
   xx(:,i) = lorenz40_solver(xx(:,i-1),KK,F,dt);
end

%---now run the TLM and test the adjoint immediately after each time step
t = 0:dt:Tend;
dx = zeros(KK,length(t))+NaN;
dx(:,1) = pert;
for i = 2:1:iend;
   xin=dx(:,i-1);
   xmean=xx(:,i-1);
   xout=lorenz40_tlm(xin,xmean,KK,F,dt);
   dx(:,i)=xout;
   % --- Test the adjoint for each time step of the TLM
   tlmout=xout;
   xinadj=lorenz40_adj(tlmout,xmean,KK,F,dt);
   LHS=xout'*xout;
   RHS=xin'*xinadj;
   disp (num2str([LHS RHS],15));   
end

echo on
% Test adjoint of full model run
echo off

% --- forward code ---
dx(:,1) = pert;
tlmin = dx(:,1);
for i = 2:1:iend;
   dx(:,i)=lorenz40_tlm(dx(:,i-1),xx(:,i-1),KK,F,dt);
end
tlmout = dx(:,iend);

% --- adjoint code ---
adjin = tlmout;
for i = iend:-1:2;
   dx(:,i-1)=lorenz40_adj(dx(:,i),xx(:,i-1),KK,F,dt);
end
adjout = dx(:,1);

echo on
% --- adjoint test ---
% for Y = L X the adjoint is X* = L* Y
% The adjoint test checks that <Y,LX> = <L*Y,X>
% where lhs = <L*Y,X> and rhs = <Y,LX> 
lhs = tlmin'*adjout;
rhs = tlmout'*adjin;
disp (num2str([lhs rhs],15));
echo off



