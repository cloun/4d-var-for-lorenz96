%--------------------------------------------------------------
function xin = lorenz40_adj(xout,xinm,KK,F,dt)
%     
%integrate the two-timescale 1995 Lorenz model forward using a 4th order RK scheme
% 23 December 2003, Lisa Neef


nv = size(xinm);
dX = zeros(nv);
x1 = zeros(nv);
x2 = zeros(nv);
x3 = zeros(nv);
x4 = zeros(nv);
fp = zeros(nv);
x1m = zeros(nv);
x2m = zeros(nv);
x3m = zeros(nv);
x4m = zeros(nv);
fpm = zeros(nv);
w1 = 1.0/6.0;
w2 = 1.0/3.0;
w3 = 1.0/3.0;
w4 = 1.0/6.0;

%---Mean trajectory
xxm1 = xinm;
fpm = lorenz40_rhs(xxm1,KK,F);
x1m = dt*fpm;
xxm2 = xinm + 0.5*x1m;
fpm = lorenz40_rhs(xxm2,KK,F);
x2m = dt*fpm;
xxm3 = xinm + 0.5*x2m;
fpm = lorenz40_rhs(xxm3,KK,F);
x3m = dt*fpm;
xxm4 = xinm + x3m;

%---Adjoint of perturbation

%xout = xin + w1*x1 + w2*x2 + w3*x3 + w4*x4;
xin=zeros(nv);
x4 = x4 + w4*xout;
x3 = x3 + w3*xout;
x2 = x2 + w2*xout;
x1 = x1 + w1*xout;
xin = xin + xout;
xout = 0.0;

fp = dt*x4;
dX = lorenz40_rhs_adj(fp,xxm4,KK,dt);
x3 = x3 + dX;
xin = xin + dX;

fp = dt*x3;
dX = lorenz40_rhs_adj(fp,xxm3,KK,dt);
x2 = x2 + 0.5*dX;
xin = xin + dX;

fp = dt*x2;
dX = lorenz40_rhs_adj(fp,xxm2,KK,dt);
x1 = x1 + 0.5*dX;
xin = xin + dX;

fp = dt*x1;
dX = lorenz40_rhs_adj(fp,xxm1,KK,dt);
xin = xin + dX;