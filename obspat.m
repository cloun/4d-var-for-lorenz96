function [hh,io] = obspat(nobs, jdim )

if (nobs < 0.)
  jmax = jdim/2;
  del = 1;
else
  jmax = jdim;
  del = nobs;
end
num = jmax/del;  
hh   = zeros(num,jdim);
io   = zeros(num,1);

j=1;
for i=1:del:jmax 
  hh(j,i) = 1.0;
  io(j) = i;
  j = j + 1;
end

end
