
%Code to run the Lorenz 40-component model (which isnt necessarily 40 components)
	% this code runs 2 trajectories with slightly different initial conditions.
	% Lisa Neef, revised 1 Feb 2004

clear all;
canned = 0;			% choose whether to use matlab's solvers or lisa's

KK = 36;
F = 8.;
dt = 0.05;
%iend = 720;			% integration time in timesteps
iend = 960;
Tend = iend*dt;

%X0 = zeros(KK,1);
%for j = 1:KK
%    X0(j) = randn(1);
%end

% --call initial conditions out of a pre-saved vector:
	[X0, X0pert] = readvars('stdInitVals.csv');

% %--or generate some random initial conditions
% 	X0 = randn(KK,1);
% 	es = 0.1;
% 	X0pert = X0+es*randn(size(X0));

if canned == 1;
%    tspan = [0 Tend];
%    Options = odeset('RelTol',10^-6,'AbsTol',10^-8);
%    [t1,xx] = ode45(@lorenz40_rhs1,tspan,X0,Options,F,KK);
%    [t2,x2] = ode45(@lorenz40_rhs1,tspan,X0pert,Options,F,KK);
else

    %---manual 4th order RK scheme:
    t = 0:dt:Tend;
    xx = zeros(KK,length(t))+NaN;
    x2 = xx;
    xx(:,1) = X0;
    x2(:,1) = X0pert;
    for i = 2:1:iend;
	xx(:,i) = lorenz40_solver(xx(:,i-1),KK,F,dt);
	x2(:,i) = lorenz40_solver(x2(:,i-1),KK,F,dt);
    end
tdays = t*5;
end

% now plot the results:

figure(1)
subplot(2,1,1)
plot(10*(1:KK),xx(:,1),10*(1:KK),x2(:,1)),title('Initial Conditions'),axis([0 KK*10 -10 15])
legend('Initial State', 'Perturbed Initial State')
subplot(2,1,2)
plot(10*(1:KK),xx(:,iend),10*(1:KK),x2(:,iend)),title('Final Conditions'),axis([0 KK*10 -10 15])
legend('Non-perturbed Run','Perturbed Run')
xlabel('x (latitude)')

figure(2)
subplot(2,1,1)
plot(t*4,xx(1,:),t*4,x2(1,:)),xlabel('time'),title('Time Evolution of 2 Points')
ylabel('Point 1')
legend('run 1','run 2')
subplot(2,1,2)
plot(t*4,xx(KK/2,:),t*4,x2(KK/2,:)),xlabel('time (days)')
legend('run 1','run 2')
ylabel(['Point ' num2str(KK/2)])
