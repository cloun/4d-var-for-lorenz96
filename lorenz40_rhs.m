%--------------------------------------------------------------
function dXdt = lorenz40_rhs(X,KK,F)

%this is the RHS of 40-component lorenz model - for the homemade RK scheme.
    % 22 December 2003, Lisa Neef

dXdt = zeros(KK,1);  


%--first the k=1,2 terms:
dXdt(1)		= -X(KK-1)*X(KK) + X(KK)*X(2) - X(1) +F;
dXdt(2)		= -X(KK)*X(1) + X(1)*X(3) - X(2) +F;

%--now loop over the other terms
for k = 3:KK-1
    dXdt(k)	= -X(k-2)*X(k-1) + X(k-1)*X(k+1) - X(k) + F;
end

%--now the k = 40 term:
dXdt(KK)	= -X(KK-2)*X(KK-1) + X(KK-1)*X(1) - X(KK) +F;

