function grd = gradcost(w)
%
%      Input:
%      tobs - observation frequency
%      nobs - no of observation
%      wo - observations = truth + random error
%      hh - observation operator
%      wa - initial background state
%      Binv - inverse of background error cov matrix
%      rr - obs error variance
%     
%      Output:
%      cost - cost function
%      kcnt - 

global nobs tobs hh rrinv rrinvsq Binv wobs nobrep
global dt Tfinal wb
wi    = w;
wbin  = wb;
bdif  = wi-wbin;
it    = nobrep;
jdim  = size(w,1);
grd   = zeros(jdim,1);

tobsu=fix(tobs/dt+0.5);

for t = Tfinal:-dt:dt
      chk = fix(t/dt + 0.5);
      mod0=mod(chk,tobsu);
%     disp (num2str([t mod0]));

%  --- If obs available, add contribution to gradient ---
   if ( mod0 == 0 ),

%     get trajectory from initial state iterate
      w = wi;
      for tt = dt:dt:t
         w = upwind ( w, tt );
      end 

%     disp (num2str([t it]));
      wo = wobs(1:nobs,it);
      innov = wo-hh*w;
      % Complete and uncomment the following line
      grd  = grd - hh' * rrinv * innov;
      it = it - 1;
   end  

%  --- propagate gradient of obs term at time t backward in time ---
   tmp = upwindad ( grd, t );
   grd = tmp;

end   %  End Main Time Loop

%  --- add contribution from initial time to gradient ---
% Complete and uncomment the following line
grd = grd + Binv*bdif ;

