\documentclass[12pt]{article}
\usepackage{extsizes}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{float}
\usepackage{geometry}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{wrapfig}
\usepackage[style = chem-acs]{biblatex}
\addbibresource{./PHY2506_Final_Project.bib}
\geometry{a4paper, portrait, margin=.8in}

\newcommand{\E}[1]{\cdot 10 ^{#1}}

\title{Assimilation of the N-Component Lorenz Model using a Strong Constraint 4D-Var Scheme}
\author{Daniel Jacob | 1006482058}
\date{December 2022}

\begin{document}
\maketitle
\section*{Introduction to the N-Component Lorenz Model \autocite{lorenz96}}
This toy model was developed by Lorenz in 1995, and intended to be a simple representation of the atmosphere. The model consists of a single variable defined on a periodic 1-D spatial grid of K equidistant points. The evolution of each point is given by Equation \ref{eq:modelEvolution}, where $F$ is a constant forcing. 
\begin{equation} \label{eq:modelEvolution}
    \frac{d}{dt}X_k = -X_{k-2}X_{k-1} + X_{k-1}X_{k+1} - X_k + F
\end{equation} \noindent
While simplistic, the model reproduces some dynamics of the atmosphere. For example, the quadratic terms somewhat replicate advection terms in weather forecast models, and the linear term can be thought of as an addition diffusion term. The forcing can represent real forcings such as sea surface temperature or topographical forcing.
\section*{Methodology}
The model was integrated using the Runge-Kutta 4 scheme. The time step for the simulation was set to 0.05 in dimensionless units, which translates to 6 hours. The model was run with 36 gridpoints, which can be likened to the measurement of a single variable (such as temperature) along a single latitude circle, with each gridpoint being at 10$^\circ$ intervals. Investigation of model behaviour was observed by integrating the model with two initial states, one that is a random $10\%$ perturbation of the other. The divergence of the two solutions from eachother provided a qualitative view on the non-linearity of the model. The evolution of specific grid points in time was also observed, providing another qualitative measure of the behaviour of the system.
\newline \indent
The assimilation was performed using a strong constraint 4D-Var scheme. The background covariance matrix had a Gaussian structure, with a default decorrelation length of 40 degrees (4 gridpoints).  A contour of the covariance matrix is shown in Figure \ref{fig:CovMatrixDefault}. The background error was set to 1.0 by default, and the effects of the background error on the analysis were studied by varying these values. The model was assumed to be perfect (no model error) for the initial simulations and assimilations, however the effects of model error on the effectiveness of the assimilation were also studied.
\newpage
\begin{wrapfigure}{l}{0.45\textwidth}
    \begin{center}
        \includegraphics[width = .45\textwidth]{CovMatrixDefault.png}
    \end{center}
    \caption[]{Default Covariance Matrix Structure}
    \label{fig:CovMatrixDefault}
\end{wrapfigure}
Observations were by default made at every gridpoint, and the observation frequency was set to once every time step by default. Again, these values were modified to study their effects on the assimilation. The observation covariance matrix was simply the identity matrix scaled by a observation error factor. The observation error was also set to 1.0 by default, and later varied to determine the effect on the analysis. Observations for use in the assimilation were created by adding a random error (selected from a normal distribution scaled by the observation error) to the true state. 
\\
\indent The cost function used to optimize the state is given in Equation \ref{eq:CostFunc}. The cost function was minimized using the prepackaged Matlab fminsearch routine. The window for optimization was the entire integration period by default, however the assimilation was also run with multiple windows per integration period and the effects on the analysis were studied.
\begin{equation} \label{eq:CostFunc}
    J(x_0) = \frac{1}{2}\sum_{k=1}^N \left( [y_k - H x_k]^T R^{-1}[y_k - Hx_k] \right) + \frac{1}{2}(x_0 - x_0^f)(P_0^f)^{-1}(x_0-x_0^f)
\end{equation} \noindent
$y_k$ is an observation made at time step $k$ (if there was one), $H$ is the observation matrix that maps the grid points to the observation and $x_k$ is the analysis at time step $k$, this is determined by evolving $x_0$ using the model dynamics. $H$ is an identity matrix with the default observation spacing of 1 since all the grid points are mapped to a single observation. $R^{-1}$ is the inverse of the observation error covariance matrix. The observations errors were assumed to be uncorrelated (so the covariance matrix was a scaled identity matrix).
The comparison of the analysis to the truth was done by taking the RMS difference between the two, and by qualitative analysis of the final results.
\section*{Results}
\subsection*{Exploring Model Dynamics}
The model was integrated with various forcing values to view the different regimes of behaviour.
\subsubsection*{Stable Regime} 
For small values of $F$, all the gridpoints approached $F$. This behaviour transitioned into a periodic behaviour above around $F = 1$. A few examples are shown in Figures \ref{fig:F0.2Behaviour} and \ref{fig:F0.8Behaviour}. We can see even with $F=0.8$ that the model initially oscillated before settling into its stable state. It also took much longer for for $F = 0.8$ case to settle down, around 120 days as opposed to 20 days in the $F=0.2$ case. 
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F0.2-InitialFinalState.png}
        \caption[]{Initial and Final States for $F = 0.2$ \\ (Integrated for 240 timesteps)}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F0.2-PointEvolution.png}
        \caption[]{Evolution of Point 1 (0 degrees) and Point 18 (170 degrees)}
    \end{subfigure}
    \caption[]{Model Behaviour for $F = 0.2$}
    \label{fig:F0.2Behaviour}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F0.8-InitialFinalState.png}
        \caption[]{Initial and Final States for $F = 0.8$ \\ (Integrated for 960 timesteps)}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F0.8-PointEvolution.png}
        \caption[]{Evolution of Point 1 (0 degrees) and Point 18 (170 degrees)}
    \end{subfigure}
    \caption[]{Model Behaviour for $F = 0.8$}
    \label{fig:F0.8Behaviour}
\end{figure}
\subsubsection*{Periodic Regime}
For $ 1 \leq F \lessapprox 4.5$ the system exhibited periodic behaviour. This can be observed in the examples show in Figures \ref{fig:F1.5Behaviour} and \ref{fig:F4.0Behaviour}. The amount of time it took for the points to settle into the periodic behaviour increased and the period of oscillation decreased as $F$ increased. The oscillations also deformed slightly as $F$ increased. The deformation of the waveform combined with the reduction in period of oscillations may be the cause of the chaotic behaviour of the model as $F$ increased.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F1.5-InitialFinalState.png}
        \caption[]{Initial and Final States for $F = 1.5$ \\ (Integrated for 960 timesteps)}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F1.5-PointEvolution.png}
        \caption[]{Evolution of Point 1 (0 degrees) and Point 18 (170 degrees)}
    \end{subfigure}
    \caption[]{Model Behaviour for $F = 1.5$}
    \label{fig:F1.5Behaviour}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.0-InitialFinalState.png}
        \caption[]{Initial and Final States for $F = 4.0$ \\ (Integrated for 960 timesteps)}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.0-PointEvolution.png}
        \caption[]{Evolution of Point 1 (0 degrees) and Point 18 (170 degrees)}
    \end{subfigure}
    \caption[]{Model Behaviour for $F = 4.0$}
    \label{fig:F4.0Behaviour}
\end{figure}
\subsubsection*{Chaotic Regime}
For $F \gtrapprox 4.5$ the system exhibited chaotic behaviour. Some examples of this are seen in Figures \ref{fig:F4.8Behaviour} and \ref{fig:F8.0Behaviour}. We can see in Figure \ref{fig:F4.8-PointEvolution} that close to $F = 4.5$ the system exhibited periodic behaviour for a little bit before it descended into chaos. As $F$ increased, the system became increasingly chaotic, with the perturbed trajectory deviating from the original trajectory more rapidly and more wildly.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-InitialFinalState.png}
        \caption[]{Initial and Final States for $F = 4.8$ \\ (Integrated for 960 timesteps)}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-PointEvolution.png}
        \caption[]{Evolution of Point 1 (0 degrees) and Point 18 (170 degrees)}
        \label{fig:F4.8-PointEvolution}
    \end{subfigure}
    \caption[]{Model Behaviour for $F = 4.8$}
    \label{fig:F4.8Behaviour}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-InitialFinalState.png}
        \caption[]{Initial and Final States for $F = 8.0$ \\ (Integrated for 960 timesteps)}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-PointEvolution.png}
        \caption[]{Evolution of Point 1 (0 degrees) and Point 18 (170 degrees)}
    \end{subfigure}
    \caption[]{Model Behaviour for $F = 8.0$}
    \label{fig:F8.0Behaviour}
\end{figure}
\subsection*{Assimilation}
Assimilations were initially done with the stable and periodic regimes to check the validity of the 4D-Var scheme. To ensure reproducibility of results, three different initial conditions (along with perturbations) were generated and saved. These saved values were used when making changes to the assimilation scheme/error values to quantitatively observe changes to the analysis. The initial conditions and their perturbations are shown below in Figure \ref{fig:initStates}. The "true" initial state was the unperturbed state, and the initial background guess was the perturbed state.
\\
The analysis for some cases were compared to the evolved background to determine whether the optimization actually helped with the analysis. The optimized initial states were also compared to the initial background and truth, and the evolution of two points in time for the analysis and truth were compared.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{initStates1.png}
        \caption[]{Initial Conditions 1}
        \label{fig:initStates1}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{initStates2.png}
        \caption[]{Initial Conditions 2}
        \label{fig:initStates2}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{initStates3.png}
        \caption[]{Initial Conditions 3}
        \label{fig:initStates3}
    \end{subfigure}
    \caption[]{Default Initial States}
    \label{fig:initStates}
\end{figure}
\subsubsection*{Stable Regime}
Naturally, for the stable regime the settings could be set incredibly unfavourably and the analysis would still converge to the truth since all gridpoints converge to the value of $F$ regardless of initial conditions. An example for $F = 0.8$ where the simulation is ended before the solution settles into a constant value is shown in Figure \ref{fig:F0.8-Analysis}. The observation error was set to 5, with observations only on the left half of the domain, and observations made every 5 timesteps. The evolution of the points showed that the analysis quickly converged to the truth after around 3 days and remained there long before the model settled down into constant behaviour. Interestingly, for favourable conditions (all default error values) the usage of the background as the initial state without assimilation gave slightly better results for all the initial conditions (the RMS error for the background evolution was around $10-15\%$ better than the analysis although both of their RMS errors were of order $1\E{-3}$). It appears that for simple model behaviour, the perturbed background is a much better estimate of the initial state than the assimilation provides. The additional observations seem to introduce some extra noise that the assimilation fits itself to.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F0.8-AFinal.png}
        \caption[]{Final State Comparison}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F0.8-AEvol.png}
        \caption[]{Comparison of evolution of two points}
    \end{subfigure}
    \caption[]{$F=0.8$ case with unfavourable measurement circumstances}
    \label{fig:F0.8-Analysis}
\end{figure}
\subsubsection*{Periodic Regime}
For the periodic regime, the behaviour was slightly more interesting.
The different initial states of the model still converged to a consistent behaviour but instead of all gridpoints converging to a single value, each gridpoint evolved periodically in time. 
In this case the analysis RMS error was around 10 times lower than the evolved background RMS error for all initial conditions, indicating a clear benefit from performing an assimilation instead of simply evolving the background state. However, interestingly the optimized initial state was always further off from the truth than the background was (the optimized state had an RMS difference from the truth around $50\%$ higher than the background), this can be seen in Figure \ref{fig:F4.0-AInitial}. It appears that the initial state can be optimized to fit the observations well, without necessarily matching the true initial state perfectly.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.0-AInitial-nobg.png}
        \caption[]{Initial State Comparison}
        \label{fig:F4.0-AInitial}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.0-AEvol.png}
        \caption[]{Final State Comparison}
        \label{fig:F4.0-AFinal}
    \end{subfigure}
    \caption[]{Assimilation of $F = 4.0$ for 96 days}
\end{figure} \noindent
Following the evolution of two points showed that the analysis rather quickly converged to the truth, much like in the stable regime.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.0-AEvol.png}
        \caption[]{Comparison of point evolution for $F = 4.0$}
        \label{fig:F4.0-AEvol}
    \end{subfigure}
\end{figure}
\subsubsection*{Chaotic Regime}
The assimilation was first done with the less chaotic $F = 4.8$ case. When the entire integration window was used for the assimilation, the analysis fit the truth quite poorly.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-AFinal-nobg.png}
        \caption[]{Comparison of Final truth}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-AEvol.png}
        \caption[]{Comparison of point evolution of truth and analysis}
        \label{fig:F4.8-AEvol}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-ARMS.png}
        \caption[]{Evolution of RMS error of analysis over time}
        \label{fig:F4.8-ARMS}
    \end{subfigure}
    \caption[]{Assimilation of $F = 4.8$ for 240 days}
    \label{fig:F4.8-Analysis}
\end{figure} \noindent
While the final state analysis appeared to follow the general structure of the truth, the evolution of the points shown in Figure \ref{fig:F4.8-AEvol} showed that the analysis appreciably deviated from the truth after around 20 days. The RMS error for the final state was certainly better than simply evolving the background (the background error RMS was around 3, while the analysis RMS error was 1.6), however it left a lot to be desired. 
\\
Instead of performing the assimilation over the entire period, the integration period was split into multiple assimilation windows. When splitting the window into 100 time-step assimilation periods (20 days), a clear improvement in the assimilation was seen. Increasing the assimilation window larger than 100 timesteps resulted in a decrease in quality, possibly due to too many observations to efficiently optimize the cost function.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-AFinal-100TS.png}
        \caption[]{Comparison of final analysis and truth}
        \label{fig:F4.8-AFinal}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-AEvol-100TS.png}
        \caption[]{Comparison of point evolution of truth and analysis}
        \label{fig:F4.8-AEvol-100TS}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F4.8-ARMS-100TS.png}
        \caption[]{Evolution of RMS error of analysis over time}
        \label{fig:F4.8-ARMS-100TS}
    \end{subfigure}
    \caption[]{Assimilation of $F = 4.8$ over 240 days with 20 day assimilation windows}
    \label{fig:F4.8-Analysis-100TS}
\end{figure} \noindent
The analysis has clearly hugely benefited from the shorter assimilation windows. Both the final state, and the overall evolution of the two points shown in Figures \ref{fig:F4.8-Analysis-100TS}a and b were much closer to the truth. The RMS of the final state was significantly better than before (new RMS error was 0.09), but there are spikes in the RMS error every 20 days shown in Figure \ref{fig:F4.8-ARMS-100TS}. Each assimilation resets the initial state of each window to better fit the observations in the window, and it appears that the initial state rarely fits the truth even though the evolution of the initial state fits the observations quite well. This appears to be a reappearance of the behaviour seen in the whole-period assimilations for the periodic regime, where the initial state could be optimized to fit observations well without fitting the initial truth particularly well.
\subsubsection*{Study of $F = 8.0$ Case and Introducing Deficiencies in the Assimilation}
The assimilation of the $F = 8.0$ case required shorter assimilation windows. With 20 timestep assimilation windows the analysis fit quite well to the truth.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-20TS-nobg.png}
        \caption[]{Final State}
        \label{fig:F8.0-AFinal}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-20TS.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-20TS.png}
        \caption[]{RMS error evolution}
        \label{fig:F8.0-ARMS}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-20TS-nobg.png}
        \caption[]{Initial State}
        \label{fig:F8.0-AInitial}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$, 20 timestep assimilation windows}
    \label{fig:F8.0-Analysis}
\end{figure} \noindent
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-10TS.png}
        \caption[]{RMS error evolution}
        \label{fig:F8.0-ARMS-10TS}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-10TS.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol-10TS}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$, 10 timestep assimilation windows}
    \label{fig:F8.0-Analysis-10TS}
\end{figure} \noindent
There were once again spikes in the RMS error at the beginning of each assimilation window, and the optimized first initial state was further from the truth than the initial background was (an RMS error of 0.32 compared to 0.1 for the background). 
Shortening the assimilation window to 10 timesteps did not provide an appreciable decrease in the RMS error, and the increased number of windows resulted in more spikes in the RMS error so in general it was preferable to use the largest assimilation window that provided a reasonable result (20 timesteps in this case). These assimilations were still in rather favourable conditions. Observations were made at every gridpoint, at every timestep, and the background and observation errors were set to 1.
\newline
Increasing the background error had almost no effect on the analysis. Each initial state was optimized over the assimilation windows anyway, and so a large background error simply resulted in the assimilation utilizing the observations more for the initial state. Because of this, there was a larger deviation from the truth in the optimized initial states (seen in Figure \ref{fig:F8.0-AInitial-initErrorComparison}), this resulted in slightly larger initial peaks in the RMS error at the beginning of each window however the quality of the analysis at other times was not impacted significantly.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-20TS-nobg.png}
        \caption[]{Initial State (Default Values)}
        \label{fig:F8.0-AInitial-Default}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-initError5.png}
        \caption[]{Initial State (Large Background Error)}
        \label{fig:F8.0-AInitial-initError5}
    \end{subfigure}
    \caption[]{Comparing Optimized Initial States}
    \label{fig:F8.0-AInitial-initErrorComparison}
\end{figure} \noindent
Increasing the observation spacing had a larger effect on the assimilation. With observations at every 3 gridpoints, the assimilation suffered a noticeable decrease in quality. This is likely because the decreased number of observations resulted in poorer optimization of the initial state. The increase in observation sparsity could clearly be seen in the optimized initial state (shown in Figure \ref{fig:F8.0-AInitial-obsSpacing3}), the optimized initial conditions much more clearly differed from the truth and background than in previous cases.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsSpacing3.png}
        \caption[]{Final State (RMS = 0.58)}
        \label{fig:F8.0-AFinal-obsSpacing3}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-obsSpacing3.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol-obsSpacing3}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-obsSpacing3.png}
        \caption[]{RMS error evolution}
        \label{fig:F8.0-ARMS-obsSpacing3}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-obsSpacing3.png}
        \caption[]{Initial state}
        \label{fig:F8.0-AInitial-obsSpacing3}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$, with sparse observations (observations every 3 gridpoints)}
\end{figure} \noindent
Increasing the observation error to 5 decreased the quality of the assimilation significantly. The reduced quality of observations resulted in the optimization of the cost functon less effectively fitting the truth. Smaller increases in the observation error (such as setting it to 3 or 2) resulted in a reasonable assimilation, however there were still noticeable differences between the truth and the analysis (shown in Figure \ref{fig:F8.0-AFinal-obsErrorComparison}). Neither increasing nor decreasing the size of the assimilation windows had an appreciable positive effect on the assimilation.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsError5.png}
        \caption[]{Final State (RMS error 1.18)}
        \label{fig:F8.0-AFinal-obsError5}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-obsError5.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol-obsError5}
    \end{subfigure}
\end{figure}
\begin{figure}[H]
    \ContinuedFloat
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-obsError5.png}
        \caption[]{RMS error evolution}
        \label{fig:F8.0-ARMS-obsError5}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-obsError5.png}
        \caption[]{Initial state}
        \label{fig:F8.0-AInitial-obsError5}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$, with large observation error (obs error = 5)}
    \label{fig:F8.0-Analysis-obsError5}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsError3.png}
        \caption[]{Final State with observation error = 3 (RMS Error = 0.5)}
        \label{fig:F8.0-AFinal-obsError3}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsError2.png}
        \caption[]{Final State with observation error = 2 (RMS Error = 0.3)}
        \label{fig:F8.0-AFinal-obsError2}
    \end{subfigure}
    \caption[]{Comparing final states with differing observation errors}
    \label{fig:F8.0-AFinal-obsErrorComparison}
\end{figure} \noindent
Decreasing the observation frequency to once every 10 timesteps resulted in a decrease in the quality of analysis, however not as much as the increase in observation error did. The decreased number of observations per window impacted how well the optimized initial state would perform, and because of the decreased number of observations sudden changes in the trajectory of the truth resulted in large RMS errors which may take multiple assimilation windows to correct (which could be seen in the RMS evolution at around 100 and 220 days in Figure \ref{fig:F8.0-ARMS-obsFreq10}). Less drastic decreases in observation frequency gave much more reasonable analyses, shown in Figure \ref{fig:F8.0-AFinal-obsFreqComparison}. Varying the assimilation window sizes in this case also did not have an appreciable positive effect on the analysis.
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsFreq10.png}
        \caption[]{Final State (RMS Error = 0.63)}
        \label{fig:F8.0-AFinal-obsFreq10}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-obsFreq10.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol-obsFreq10}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-obsFreq10.png}
        \caption[]{RMS Error evolution}
        \label{fig:F8.0-ARMS-obsFreq10}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-obsFreq10.png}
        \caption[]{Initial State}
        \label{fig:F8.0-AInitial-obsFreq10}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$ with time sparse observations (observations every 10 timesteps)}
\end{figure}
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsFreq8.png}
        \caption[]{Final State (RMS Error = 0.36) with observations every 8 timesteps}
        \label{fig:F8.0-AFinal-obsFreq8}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-obsFreq5.png}
        \caption[]{Final State (RMS Error = 0.32) with observations every 5 timesteps}
        \label{fig:F8.0-AFinal-obsFreq5}
    \end{subfigure}
    \caption[]{Comparing final states with different observation frequencies}
    \label{fig:F8.0-AFinal-obsFreqComparison}
\end{figure} \noindent
Introducing model error affected the assimilation similarly to increasing the observation error. This is to be expected, since the model error was added in the same way that the observation error was, except that the covariance matrix for the model error was the same as the covariance matrix for the background error as opposed to the scaled identity matrix that was used for the observation error. 
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-modelError1.png}
        \caption[]{Final State (RMS Error = 1.05)}
        \label{fig:F8.0-AFinal-modelError1}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-modelError1.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol-modelError1}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-modelError1.png}
        \caption[]{RMS error evolution}
        \label{fig:F8.0-ARMS-modelError1}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-modelError1.png}
        \caption[]{Initial State}
        \label{fig:F8.0-AInitial-modelError1}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$, with model error (model error = 1)}
    \label{fig:F8.0-Analysis-modelError1}
\end{figure} \noindent
Isolating observations to the left side of the grid naturally reduced the quality of the analysis quite a bit. Strictly according to the RMS error it had the largest impact on the analysis, however on inspection of the final state it is clear that the absense of observations on the right side didn't affect the analysis on the left side to the point where it was unusable. As seen in Figure \ref{fig:F8.0-AEvol-leftObs}, the analysis of point 5, which was on the left side and whose evolution did not directly depend on any of the points on the right side, rather closely matched the truth. While the analysis for point 25 which is on the right side and whose evolution directly depends on points exclusively on the right side matched the truth almost nowhere. If this is kept in mind, the results from the analysis can still be utilized. 
\begin{figure}[H]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AFinal-leftObs.png}
        \caption[]{Final State (RMS Error = 1.7)}
        \label{fig:F8.0-AFinal-leftObs}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AEvol-leftObs.png}
        \caption[]{Evolution of two points}
        \label{fig:F8.0-AEvol-leftObs}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-ARMS-leftObs.png}
        \caption[]{RMS error evolution}
        \label{fig:F8.0-AEvol-leftObs}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics*[width = \textwidth]{F8.0-AInitial-leftObs.png}
        \caption[]{Initial State}
        \label{fig:F8.0-AInitial-leftObs}
    \end{subfigure}
    \caption[]{Assimilation for $F = 8.0$, observations only on left side}
    \label{fig:F8.0-Analysis-leftObs}
\end{figure}
\section*{Conclusions}
Overall for the highly chaotic N-component Lorenz system, the quality, frequency, and quantity of observations had the greatest impact on the analysis. Naturally, for highly chaotic systems if the initial background guess even slightly deviates from the truth it is almost worthless on longer timescales. In such cases, shorter assimilation windows allow the analysis to better correct for deviations. The larger the assimilation window, the smaller the solution space of acceptable initial states becomes, and as such the correct minimization of the cost function becomes more difficult. However, excessively short assimilation windows can overfit the initial state to the observations and result in a very poor fit of the initial state to the true initial state. The selection of assimilation windows is a trade-off between the quality of the initial state and the quality of the analysis within the window. Strong constraint 4D-Var proves to be a viable assimilation method for highly chaotic systems if there is a robust set of observations, however in cases where observations are sparse or of low quality it may be more fruitful to pursue other avenues of investigation.
\nocite{*}
\printbibliography
\end{document}