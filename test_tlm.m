
%Code to test the TLM of the Lorenz 40-component model (which isnt necessarily 40 components)
	% this code runs 2 trajectories with slightly different initial conditions.
	% Saroja Polavarapu Feb. 15, 2004

clear all;
clf,figure(1);
clf,figure(2);
clf,figure(3);
clf,figure(4);

KK = 72;				%no. of gridpoints along a latitude circle (Grid spacing of 10 deg.)
F = 8.0;				%forcing
dt = 0.05;			%time step (6 hours or 0.25 days)
iend = 72;			%total number of timesteps to run
Tend = iend*dt;	%length of integration in nondimensional time units
%To convert time to days, multiply by 5
%To convert time to years, divide by 72
cday=5.0;			%factor to convert time to days
cyear=1.0/72.0;	%factor to convert time to years

%--generate some random initial conditions
X0 = 2+randn(KK,1);
es = 0.1;
pert=es*randn(size(X0));
X0pert = X0+pert;

%---manual 4th order RK scheme:
t = 0:dt:Tend;
xx = zeros(KK,length(t))+NaN;
x2 = xx;
xx(:,1) = X0;
x2(:,1) = X0pert;
for i = 2:1:iend;
   xx(:,i) = lorenz40_solver(xx(:,i-1),KK,F,dt);
   x2(:,i) = lorenz40_solver(x2(:,i-1),KK,F,dt);
end
ndif=x2-xx;

%---now run the TLM
dx = zeros(KK,length(t))+NaN;
dx(:,1) = pert;
for i = 2:1:iend;
   dx(:,i) = lorenz40_tlm(dx(:,i-1),xx(:,i-1),KK,F,dt);
end

% now plot the results:

tdim=t*cday;		%t in years

% Plot initial and final conditions as a function of x
figure(1),subplot(2,1,1)
plot(1:KK,xx(:,1),1:KK,x2(:,1)),title('Initial Conditions'),axis([0 KK -10 15])
subplot(2,1,2)
plot(1:KK,xx(:,iend),1:KK,x2(:,iend)),title('Final Conditions'),axis([0 KK -10 15])
legend('run 1','run 2')
xlabel('x')

% Plot time series at two points in space
figure(2),subplot(2,1,1)
plot(tdim,xx(1,:),tdim,x2(1,:)),xlabel('time (days)'),title('Time Evolution of 2 Points')
ylabel('Point 1')
subplot(2,1,2)
plot(tdim,xx(KK/2,:),tdim,x2(KK/2,:)),xlabel('time (days)')
legend('run 1','run 2')
ylabel(['Point ' num2str(KK/2)])

% Plot initial error and final error as a function of x
figure(3),subplot(4,1,1)
plot(1:KK,ndif(:,1),1:KK,dx(:,1))
title('Initial Difference'),legend('Mean-pert','TLM initial'),xlabel('x')
subplot(4,1,2)
plot(1:KK,ndif(:,iend),1:KK,dx(:,1))
title('Final Difference'),legend('Mean-Pert','TLM final'),xlabel('x')
% Plot time series of error evolution
subplot(4,1,3)
plot(tdim,ndif(1,:),tdim,dx(1,:))
xlabel('time (days)'),ylabel('Point 1'),title('Time series of difference')
subplot(4,1,4)
plot(tdim,ndif(KK/2,:),tdim,dx(KK/2,:))
xlabel('time (days)'),ylabel(['Point ' num2str(KK/2)]),legend('nonlinear diff','TLM')

% Compute and plot RMS error over whole grid
tmp=ndif.*ndif;
rms=sqrt(mean(tmp,1));
tmp=dx.*dx;
rmstlm=sqrt(mean(tmp,1));

figure(4),subplot(2,1,1)
plot(tdim,rms,tdim,rmstlm)
xlabel('time (days)'),ylabel('rms error'),title('domain avg error')
legend('nonlinear diff','TLM')

% --- show NLM-TLM difference as a percentage
tlmerr=100*(abs(rms-rmstlm));
subplot(2,1,2)
plot(tdim,tlmerr)
xlabel('time (days)'),ylabel('TLM error (%)'),title('nonlinear diff - TLM')




