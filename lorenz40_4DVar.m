function [ wa, Va ] = var4d( All_Verbose, StepFinal_in, KK_in, F_in)
%VAR4D  4DVAR code for linear advection equation
%
%       Usage:  var4d ( All_Verbose, Tfinal, CN, do_filer )
%
%       Inputs:
%
%       All_Verbose   -  when diff than zero, will prompt a full set of
%                        large set of options
%       Tfinal        -  total intergration time
%       CN            -  Courant number
%       do_filter     -  0=carry estimation using 4dvar; 
%                        1=no estimation
%
%       List of questions user will be prompted to: 
%
%                                            Default values
%                                            --------------
%       Total time of integration        -     1 time unit
%
%       Courant number                   -       1
%       Initial error standard deviation -       0.05
%
%       Parameters related to Obs Pattern:
%       Observation frequency            -       dt  (every model time step)
%       Observation error standard dev.  -       0.02
%       Observation sparsity             -       every grid point
%
%       Plotting frequency               -       0 (only in the beginning
%                                                   and end of a run)
%                                                -  set this to a multiple
%                                                   of dt and you get plots
%                                                   with this frequency
%
%  25feb98  R. Todling   Initial code.
%  20feb01  S. Polavarapu Modified for 4dvar

global dt Tfinal
figure(1), clf
figure(2), clf
echo off
do_filter = 0;

dt   = 0.05; % dt is 6 hours, or 0.25 days
Tfinal = double(StepFinal_in*dt);
% multiply by 5 to convert to days, divide by 72 to convert to years


%  Define Lorentz System Parameters
%
global KK F
echo on
KK = KK_in;          %  Number of Components
F = F_in;            %  Forcing
echo off
% array containing latitude values of each point
x = 0:360/KK:360*(1-1/KK);



% Build initial condition
%

% defines initial conditions
[X0, X0Pert] = readvars('stdinitVals.csv');

% % defines initial conditions
% X0 = randn(KK, 1);
% % perturbs the initial conditions by 10%
% pertPerc = 0.1;
% X0Pert = X0 + pertPerc*randn(size(X0));

jdim = size(X0,1);
wt   = zeros(jdim,1);


%  Initialize error covariances
%
ID = eye(jdim); ZERO = zeros(jdim); 
% setting the distance for correlation drop-off (in degrees)
corrSigma = 40;
%
%  Define a correlation matrix and its Cholesky decomposition
%
Cor0 = gcorr ( 'gauss', 360, corrSigma, jdim, 0 );
[V,D]=eig(Cor0);  sCor0 = V * sqrt(D); 
%
%  Set the true state to the analytic function w0 and define
%  initial error magnitude
%
wt = X0;

stdAd = 1.; stdA=stdAd;
if (All_Verbose~=0), stdA= input('Enter initial error magnitude (1.):');
   if isempty(stdA), stdA=stdAd, end
end
B = stdA^2 * Cor0;

wa = X0Pert;

stdQd = 0; stdQ=stdQd;
if (All_Verbose~=0), stdQ= input('Enter model error magnitude(std dev, set to null):');
   if isempty(stdQ), stdQ=stdQd, end
end
qq = stdQ^2 * Cor0;

%
%  Define observing network info
%
global nobs tobs hh rrinv rrinvsq Binv wobs nobrep wb
defaultOError = 1. % default observation error
wobs=[];  % Initialize wobs matrix to null
Binv = inv(B);
tobs = 99999;
if ( do_filter == 0 ),
   
   tobs  = input('Enter observation frequency (this*dt):');
   if isempty(tobs), tobs=1; end 
   tobs   = tobs * dt;

   nobs  = input('Enter observation sparsity (obs over left-half):');
	if isempty(nobs), nobs=-jdim/2; end
    
    if (All_Verbose ~= 0)
	    stdO = input('Enter observation error std dev (1.):');
	    if isempty(stdO), stdO = defaultOError; end
    else
        stdO = defaultOError;
    end
%
%  Construct observation error covariance and observation matrix
%
	[hh,io] = obspat(nobs,jdim);
	nobs    = length(io);
	rr      = stdO^2 * eye(nobs);
	rrinv   = (1.0/stdO^2) * eye(nobs);
	rrinvsq = (1.0/stdO)   * eye(nobs);
   
end   

size(hh)

%
%  Gather I/O information
%
Plot_Nowd = 999999; Plot_Now=Plot_Nowd;
   if (All_Verbose~=0), Plot_Now  = input('Enter plotting frequency (this*dt)'); 
   if isempty(Plot_Now), Plot_Now = Plot_Nowd, end
end   
if (Plot_Now ~= 999999), figure(3), clf, end
% %
% %  Now plot some initial stuff: initial truth, initial estimate,
% %      observation pattern
% %        
ymaxT = max(wt); ymaxA = max(wa); ymax=max(ymaxT,ymaxA); 
yminT = min(wt); yminA = min(wa); ymin=min(yminT,yminA);    
% figure(1),subplot(211), hold on,plot(x,wt,'c-'),axis([x(1) x(jdim) ymin ymax]) ;
% figure(1),subplot(211), hold on,plot(x,wa,'b-');
% if(do_filter==0),
% figure(1),subplot(211), hold on,plot(x(io),zeros(length(io),1),'y*');
% figure(1),subplot(211), hold on, ...
%           legend('Truth at T0','Estimate at T0','Obs Pattern'), hold off;
% IF NO SUBFIGURE
figure(1), hold on,plot(x,wt,'c-');
figure(1), hold on,plot(x,wa,'b-');
if(do_filter==0),
figure(1), hold on,plot(x(io),zeros(length(io),1),'y*');
figure(1), hold on, ...
          legend('Truth at T0','Estimate at T0','Obs Pattern'), hold off;
else
          legend('Truth at T0','Estimate at T0');	  
end       
echo on;
pause % Strike any key for plot.
echo off;

global windowdi windowdf

% requesting how long the assimilations windows should be
defaultWindow = StepFinal_in; % default window is the whole period

windowLen = input('Enter the window length (in time steps, leave empty to assimilate over whole period):');
if isempty(windowLen), windowLen = defaultWindow; end

% starting points for the windows
windowdi = dt;
windowdf = windowLen*dt;
% this will store the optimized states
wanOpt = [wa];
wb = wa;
% storing hypothetical background state
wbEv = wb;
% storing the truth stats in time
wtArray = [wt];
%  
%  Start Main Time Loop
%
kcnt = 0;
Va   = sqrt(sum(diag(B)));
for t = dt:dt:Tfinal
   if ( mod(t,Plot_Now*dt) == 0 ),
      figure(3),plot(x,wt,'r-'), hold on;
      figure(3),plot(x,wa,'g-'), hold on;
      %echo on;
      %echo off;
   end
%
%  Evolve true state
%
   wt = real(lorenz40_solver(wt, KK, F, dt)  +  stdQ * sCor0 * randn(jdim,1));
   wtArray = [wtArray, wt];
   % evolving background state (for comparison to analysis)
   wbEv = real(lorenz40_solver(wbEv, KK, F, dt) +  stdQ * sCor0 * randn(jdim,1));
%
%  Generate observations by adding random error
%
   if ( mod(t,tobs) == 0),
      kcnt = kcnt + 1;
      wo = hh * wt  +  stdO * randn(nobs,1);
      if ( kcnt == 1 ),
         wobs = wo;
      else
         wobs = [wobs,wo];
      end
   end

% 
% If this is a window end, the window is optimized using fminsearch
%
    if ( abs(t - windowdf) <= 1e-5)
        % if its the first window, need to initialize the array of
        % optimized values by starting optimizing the background over the
        % window length
        
        wanOpt(:, end) = fminsearch(@cost, wanOpt(:, end));
        for twindow = windowdi:dt:t
            wanOpt = [wanOpt, lorenz40_solver(wanOpt(:, end), KK, F, dt)];
        end
        windowdi = t+dt;
        windowdf = windowdi+windowLen*dt-dt;
    end

end   %  End Main Time Loop
nobrep=kcnt;
nobs;
wobs;
% %
% %   Cost function
% %
% w = wa;
% wb = wa;
% 
% % using fminsearch to minimize the cost function
% options = optimset('Display','off');
% wanli = fminsearch(@cost, w, options);
% wanl = wanli;
% 
% %
% %   Integrate solution (at initial time) to get final time
% %
% for t = dt:dt:Tfinal
%     wanl = lorenz40_solver(wanl, KK, F, dt);
% end
% wanl;
% wanli;

wanl = wanOpt(:, end);
wanli = wanOpt(:, 1);

% measuring how good the optimization of the initial state is
% calculating difference between initial background state and initial state
pertDif = X0 - X0Pert;
pertRMS = rms(pertDif)
optDif = X0 - wanli;
optRMS = rms(optDif)

% measuring how the analysis final state compares to if the background was
% simply evolved
bgDif = wt - wbEv;
bgDifRMS = rms(bgDif)
analDif = wt - wanl;
analDifRMS = rms(analDif)

% getting a time RMS measure
RMSvals = wtArray - wanOpt;
RMSvals = rms(RMSvals, 1);

%
%  Always plot the file results
%
% % comparing initial states
% figure(1),subplot(211), hold on,plot(x,wanli,'g-'),hold off;
% figure(1),subplot(211), hold on, ...
%           legend('Truth at T0','Estimate at T0','Obs Pattern', 'Optimized Initial Conditions'), hold off;
% 
% figure(1), subplot(212), hold on, plot(x, pertDif, 'b-'), hold off;
% figure(1), subplot(212), hold on, plot(x, optDif, 'g-'), hold off;
% figure(1), subplot(212), hold on, ...
%     legend('Background', 'Optimization')
%     ylabel('Difference from Truth');
%     xlabel('Longitude (degrees)')
% 
% ymaxT = max(wt); ymaxA = max(wanl); ymaxN=max(ymaxT,ymaxA); ymax=max(ymax,ymaxN);
% yminT = min(wt); yminA = min(wanl); yminN=min(yminT,yminA); ymin=min(ymin,yminN);

% comparing initial states without the background
figure(1), hold on,plot(x,wanli,'g-'),hold off;
figure(1), hold on, ...
          legend('Truth at T0','Estimate at T0','Obs Pattern', 'Optimized Initial Conditions'), hold off;

ymaxT = max(wt); ymaxA = max(wanl); ymaxN=max(ymaxT,ymaxA); ymax=max(ymax,ymaxN);
yminT = min(wt); yminA = min(wanl); yminN=min(yminT,yminA); ymin=min(ymin,yminN);

% % comparing final states
% figure(2), subplot(211), hold on,plot(x,wt,'r-'), ...
%           axis([x(1) x(jdim) ymin ymax])
% figure(2), subplot(211), hold on,plot(x,wanl,'g-')
% figure(2), subplot(211), hold on, ...
%    legend('Truth at Tf','Estimate at Tf'), hold off;
% figure(2), subplot(212), hold on, plot(x, bgDif, 'b-'), hold off;
% figure(2), subplot(212), hold on, plot(x, analDif, 'g-'), hold off;
% figure(2), subplot(212), hold on, ...
%     legend('Backgroupd', 'Optimization')
%     ylabel('Difference from Truth')
%     xlabel('Longitude (degrees)')
    

% comparing final states without the background
figure(2), hold on,plot(x,wt,'r-'), hold off;
figure(2), hold on,plot(x,wanl,'g-')
figure(2), hold on, ...
    xlabel('Latitude (degrees)'), hold off;
figure(2), hold on, ...
   legend('Truth at Tf','Analysis at Tf'), hold off;

% comparing time evolution of two points
figure(3), clf
title('Time Evolution of Two Points')
figure(3), subplot(211), hold on, plot(5*(dt:dt:Tfinal), wtArray(5, 2:end), 'r-'), hold off;
figure(3), subplot(211), hold on, plot(5*(dt:dt:Tfinal), wanOpt(5, 2:end), 'g-'), hold off;
figure (3), subplot(211), hold on, ...
    ylabel('Evolution of Point 5'), hold off;
figure(3), subplot(212), hold on, plot(5*(dt:dt:Tfinal), wtArray(25, 2:end), 'r-'), hold off;
figure(3), subplot(212), hold on, plot(5*(dt:dt:Tfinal), wanOpt(25, 2:end), 'g-'), hold off;
figure (3), subplot(212), hold on, ...
    xlabel('Time (Days)')
    ylabel('Evolution of Point 25')
    legend('Truth', 'Analysis'), hold off;

% getting the RMS value's evolution in time
figure(4), clf;
title('RMS of Analysis over Time')
figure(4), hold on, ...
    xlabel('Time (days)')
    ylabel('RMS')
    plot(5*(dt:dt:Tfinal), RMSvals(2:end)), hold off;
