%--------------------------------------------------------------
function xout = lorenz40_tlm(xin,xinm,KK,F,dt)
%     
%integrate the two-timescale 1995 Lorenz model forward using a 4th order RK scheme
% 23 December 2003, Lisa Neef


nv = size(xin);
dX = zeros(nv);
x1 = zeros(nv);
x2 = zeros(nv);
x3 = zeros(nv);
x4 = zeros(nv);
fp = zeros(nv);
w1 = 1.0/6.0;
w2 = 1.0/3.0;
w3 = 1.0/3.0;
w4 = 1.0/6.0;

%---Mean trajectory
xxm1 = xinm;
fpm = lorenz40_rhs(xxm1,KK,F);
x1m = dt*fpm;
xxm2 = xinm + 0.5*x1m;
fpm = lorenz40_rhs(xxm2,KK,F);
x2m = dt*fpm;
xxm3 = xinm + 0.5*x2m;
fpm = lorenz40_rhs(xxm3,KK,F);
x3m = dt*fpm;
xxm4 = xinm + x3m;

%---Perturbation
dX = xin;
fp = lorenz40_rhs_tlm(dX,xxm1,KK);
x1 = dt*fp;

dX = xin + 0.5*x1;
fp = lorenz40_rhs_tlm(dX,xxm2,KK);
x2 = dt*fp;

dX = xin + 0.5*x2;
fp = lorenz40_rhs_tlm(dX,xxm3,KK);
x3 = dt*fp;

dX = xin + x3;
fp = lorenz40_rhs_tlm(dX,xxm4,KK);
x4 = dt*fp;
xout = xin + w1*x1 + w2*x2 + w3*x3 + w4*x4;
