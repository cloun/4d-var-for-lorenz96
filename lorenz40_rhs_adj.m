%--------------------------------------------------------------
function X = lorenz40_rhs_adj(dXdt,Xmean,KK,F)

%this is the adj RHS of TLM of 40-component lorenz model - for the homemade RK scheme.
% Saroja Polavarapu Jan. 30, 2004

X = zeros(KK,1);

%--adjoint of the k = 40 term:
%    dXdt(KK) = -Xmean(KK-2)*X(KK-1) - X(KK-2)*Xmean(KK-1) + ...
%                Xmean(KK-1)*X(1) + X(KK-1)*Xmean(1) - X(KK);

X(KK) = X(KK) - dXdt(KK);
X(KK-1)=X(KK-1) + Xmean(1)*dXdt(KK);
X(1)=X(1) + Xmean(KK-1)*dXdt(KK);
X(KK-2)=X(KK-2) - Xmean(KK-1)*dXdt(KK);
X(KK-1)=X(KK-1) - Xmean(KK-2)*dXdt(KK);
dXdt(KK)=0;

%--adjoint of loop over other terms
%   dXdt(k)	= -Xmean(k-2)*X(k-1) - X(k-2)*Xmean(k-1) + ...
%              Xmean(k-1)*X(k+1) + X(k-1)*Xmean(k+1) - X(k);

for k = KK-1:-1:3
   X(k) = X(k) - dXdt(k);
   X(k-1) = X(k-1) + Xmean(k+1)*dXdt(k);
   X(k+1) = X(k+1) + Xmean(k-1)*dXdt(k);
   X(k-2) = X(k-2) - Xmean(k-1)*dXdt(k);
   X(k-1) = X(k-1) - Xmean(k-2)*dXdt(k);
   dXdt(k)=0;
end

%--adjoint of the k=2 term:
%   dXdt(2) = -Xmean(KK)*X(1) -X(KK)*Xmean(1) + ...
%              Xmean(1)*X(3) + X(1)*Xmean(3)  - X(2);
X(2) = X(2) - dXdt(2);
X(1) = X(1) + Xmean(3)*dXdt(2);
X(3) = X(3) + Xmean(1)*dXdt(2);
X(KK)=X(KK) - Xmean(1)*dXdt(2);
X(1) = X(1) - Xmean(KK)*dXdt(2);
dXdt(2)=0;

%--adjoint of the k=2 term:
%   dXdt(1)	= -Xmean(KK-1)*X(KK) - X(KK-1)*Xmean(KK) + ...
%              Xmean(KK)*X(2) + X(KK)*Xmean(2) - X(1);

X(1) = X(1) - dXdt(1);
X(KK) = X(KK) + Xmean(2)*dXdt(1);
X(2) = X(2) + Xmean(KK)*dXdt(1);
X(KK-1)=X(KK-1) - Xmean(KK)*dXdt(1);
X(KK) = X(KK) - Xmean(KK-1)*dXdt(1);
dXdt(1)=0;

   