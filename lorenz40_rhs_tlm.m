%--------------------------------------------------------------
function dXdt = lorenz40_rhs_tlm(X,Xmean,KK)

%this is the RHS of 40-component lorenz model - for the homemade RK scheme.
    % 22 December 2003, Lisa Neef

dXdt = zeros(KK,1);  


%--first the k=1,2 terms:
dXdt(1)		= -Xmean(KK-1)*X(KK) - X(KK-1)*Xmean(KK) + ...
               Xmean(KK)*X(2) + X(KK)*Xmean(2) - X(1);
dXdt(2)		= -Xmean(KK)*X(1) -X(KK)*Xmean(1) + ...
               Xmean(1)*X(3) + X(1)*Xmean(3)  - X(2);

%--now loop over the other terms
for k = 3:KK-1
   dXdt(k)	= -Xmean(k-2)*X(k-1) - X(k-2)*Xmean(k-1) + ...
               Xmean(k-1)*X(k+1) + X(k-1)*Xmean(k+1) - X(k);
end

%--now the k = 40 term:
dXdt(KK)	= -Xmean(KK-2)*X(KK-1) - X(KK-2)*Xmean(KK-1) + ...
            Xmean(KK-1)*X(1) + X(KK-1)*Xmean(1) - X(KK);

