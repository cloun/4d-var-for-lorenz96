KK = 36;

% defines standard starting conditions

% % defines initial conditions
% X0 = randn(KK, 1);
% % perturbs the initial conditions by 10%
% pertPerc = 0.1;
% X0Pert = X0 + pertPerc*randn(size(X0));
% 
% stdVar = [X0, X0Pert];
% 
% save stdInitVals3.csv stdVar -ascii


% visualizes vectors
[X0, X0pert] = readvars('stdInitVals3.csv');

figure(1)
figure(1), hold on, plot(10:10:KK*10, X0, 'b-'), hold off;
figure(1), hold on, plot(10:10:KK*10, X0pert, 'r-'), hold off;
figure(1)
legend('True Initial State', "Perturbed Initial State")
axis([10 10*KK min(min(X0), min(X0pert)) max(max(X0), max(X0pert))])
xlabel('Longitude (degrees)')